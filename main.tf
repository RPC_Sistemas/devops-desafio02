terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  profile   = "devops"
  region = "us-west-2"
}

data "template_file" "user_data" {
  template = file("./userdata.sh")
  vars = {
    rds_endpoint  = "${aws_db_instance.ins_bd.endpoint}"
  }
}

#Criando EC2
resource "aws_instance" "ins_wp" {
  ami           = "ami-0c09c7eb16d3e8e70"
  # ami           = "ami-0149b2da6ceec4bb0"
  subnet_id     = aws_subnet.public1.id
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.sg_wp.id]
  user_data = data.template_file.user_data.rendered
  key_name = "desafio2"
  depends_on = [aws_db_instance.ins_bd]
  tags = {
    Name = "Wordpress"
  }
}

#Criando RDS
resource "aws_db_instance" "ins_bd" {
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  db_name                = "wordpress"
  username               = "adminadmin"
  password               = "adminadmin"
  parameter_group_name   = "default.mysql5.7"
  vpc_security_group_ids = [aws_security_group.sg_bd.id]
  db_subnet_group_name   = aws_db_subnet_group.subnetGroups.name
  availability_zone    = "${aws_subnet.private1.availability_zone}"
  skip_final_snapshot    = true
  publicly_accessible    = true
  identifier = "dbdesafio2"
}

resource "aws_db_subnet_group" "subnetGroups" {
  name       = "subnetgroups"
  subnet_ids = [aws_subnet.private1.id, aws_subnet.private2.id]

  tags = {
    Name = "Subnet Groups"
  }
}

# Pra compartilhar dados entre projetos terraform
# data "terraform_remote_state" "remote-state-vpc" {
#   backend = "s3"
#   config = {
#     bucket = "bucket-magno-terraform"
#     key    = "aws-vpc/terraform.tfstate"
#     region = "us-east-1"
#   }
# }

# subnet_id                   = data.terraform_remote_state.remote-state-vpc.outputs.subnet_id